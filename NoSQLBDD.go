package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	bolt "github.com/coreos/bbolt"
	_ "github.com/lib/pq"
)

type Alumne struct {
	Legajo   int
	Nombre   string
	Apellido string
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

func main() {

	//Conexion BDD PostgreSQL
	dbR, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer dbR.Close()

	//Selecciono un limite de 3 alumnos
	rows, err := dbR.Query(`select * from alumne limit 3`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	//Almaceno los alumnos en mi variable a
	var a Alumne
	var arr []Alumne
	for rows.Next() {
		if err := rows.Scan(&a.Legajo, &a.Nombre, &a.Apellido); err != nil {
			log.Fatal(err)
		}
		arr = append(arr, a)
		fmt.Printf("%v %v %v\n", a.Legajo, a.Nombre, a.Apellido)
		//fmt.Printf("%s\n", a)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}

	// BDD NoSQL
	db, err := bolt.Open("guaraní.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	for i := 0; i < len(arr); i++ {

		data, err := json.Marshal(arr[i])
		if err != nil {
			log.Fatal(err)
		}
		//fmt.Printf("%s\n", data)

		CreateUpdate(db, "alumne", []byte(strconv.Itoa(arr[i].Legajo)), data)

		resultado, err := ReadUnique(db, "alumne", []byte(strconv.Itoa(arr[i].Legajo)))

		fmt.Printf("%s\n", resultado)
	}
}
