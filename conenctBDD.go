package main

import (
	"database/sql"
	"fmt"
	"log"

	// Es necesario antes de realizar el import de una libreria, realizar el 'go get github.com/...'
	_ "github.com/lib/pq"
)

type alumne struct {
	legajo           int
	nombre, apellido string
}

func createDatabase() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(`create database guarani`)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	//createDatabase()

	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=guarani sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//_, err = db.Exec(`create table alumne (legajo int, nombre text, apellido text)`)
	if err != nil {
		log.Fatal(err)
	}

	//_, err = db.Exec(`ALTER TABLE alumne ADD CONSTRAINT alumne_pk PRIMARY KEY(legajo)`)
	if err != nil {
		log.Fatal(err)
	}

	// _, err = db.Exec(`insert into alumne values (1, 'Cristina', 'Kirchner');
	// 				  insert into alumne values (2, 'Juan Domingo', 'Perón');
	// 				  insert into alumne values (3, 'Sebastian', 'Posadas');
	// 				  insert into alumne values (4, 'Cristian', 'Posadas');
	// 				  insert into alumne values (5, 'Andres', 'Posadas');`)

	if err != nil {
		log.Fatal(err)
	}

	rows, err := db.Query(`select * from alumne`)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()
	var a alumne
	var arr []alumne
	for rows.Next() {

		if err := rows.Scan(&a.legajo, &a.nombre, &a.apellido); err != nil {
			log.Fatal(err)
		}
		arr = append(arr, a)
		fmt.Printf("%v %v %v\n", a.legajo, a.nombre, a.apellido)
	}
	if err = rows.Err(); err != nil {
		log.Fatal(err)
	}
	//Verificación de structs
	fmt.Println(arr[0].legajo, arr[0].nombre, arr[0].apellido)
	fmt.Println(len(arr))
}
